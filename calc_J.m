function [f, g] = calc_J(R_k, omega_k, bar_u)
    global Qr Qo R p Terminalcost hold_order
    
    dim_bar = size(bar_u,1);        % size of input row = 3*p
    %predict_step_num=p;             %predict_step_num=dim(barR)/dim(u_*)
    barQr = zeros(dim_bar); %mbr^{3p*3p}
    barQo = zeros(dim_bar);      %mbr^{3p*3p}
    barR = zeros(dim_bar);        %mbr^{3p*3p}
    for i = 1:p
        barQr(i*3-2:i*3,i*3-2:i*3)=Qr; % barQr=diag([Qr, Qr, ..., Qr])
        barQo(i*3-2:i*3,i*3-2:i*3)=Qo; % barQo=diag([Qo, Qo, ..., Qo])
         barR(i*3-2:i*3,i*3-2:i*3)=R;  % barR =diag([ R,  R, ..., R])
    end
    % add terminal cost
    barQr(dim_bar-2:dim_bar,dim_bar-2:dim_bar) = Terminalcost*barQr(dim_bar-2:dim_bar,dim_bar-2:dim_bar);
    
    %% calc {q, omega}_{k+1, ..., k+p}  from q_k, omega_k, u_{k, ..., k+p-1}
    [bar_R, bar_omega] = calc_state_p_step(R_k, omega_k, bar_u, hold_order);
    
    %% cost function
    bar_r=zeros(3*p,1);
    for i=1:p
       a=SO3_to_so3(bar_R(3*i-2:3*i, 1:3));
       bar_r(3*i-2:3*i)=[a(3,2); a(1,3); a(2,1)];
    end
    f = bar_r'*barQr*bar_r/2+bar_omega'*barQo*bar_omega/2+bar_u'*barR*bar_u/2;
    
    %% grad J
    %g = calc_gradJ(bar_R, bar_omega, bar_u, barQr, barQo, barR);
end
