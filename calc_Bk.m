function Bk = calc_Bk(q_k, omega_k)
    global Ts
    if isrow(omega_k)
        omega_k=omega_k.';
    end
    no = norm(omega_k);
    wk = no*Ts*0.5;
    if no>0
        gk=sin(wk)*(1/no)*omega_k;
    else
        gk=[0;0;0];
    end 
    cwk = cos(wk);
    swk = sin(wk);
    o=omega_k;
    qz=q_k(1);
    qv=q_k(2:4);
    hqv=omega_to_omegahat(qv);
   %%
    if isrow(gk)
        gk=gk.';
    end
    if isrow(omega_k)
        qv=qv.';
    end
   %%
    H=eye(3)*Ts*0.5;
    if no>0
        for n=1:3
            for m=1:3
                if n==m
                    delta=1;
                else
                    delta=0;
                end
                H(n,m)= o(n)*o(m)*(1/no)*(1/no)*Ts*0.5*cwk+(delta/no-o(n)*o(m)/no/no/no)*swk;
            end
        end
    end
    Bk(1, 1:3)   = (-1)*qz*Ts*0.5*(gk.')+(-1)*(qv.')*H; 
    Bk(2:4, 1:3) = (qz*eye(3)+hqv)*H-0.5*Ts*qv*(gk.');
end