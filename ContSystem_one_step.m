function [R_kp, omega_kp] = ContSystem_one_step(R_k, omega_k, u_k)
    global Ts
    r_k=[R_k(1,1); R_k(1,2); R_k(1,3); R_k(2,1); R_k(2,2); R_k(2,3); R_k(3,1); R_k(3,2); R_k(3,3)];
    x_0=[r_k; omega_k];
    [T,x_kp] = ode45(@(t,x)ContSystem(x,u_k),[0 Ts],x_0);
    r_kp     = x_kp(end, 1:9);
    R_kp=[r_kp(1), r_kp(2), r_kp(3);
        r_kp(4), r_kp(5), r_kp(6);
        r_kp(7), r_kp(8), r_kp(9)];
    omega_kp = x_kp(end, 10:12);
end


function [dx] = ContSystem(x,u)
    r=x(1:9);
    omega=x(10:12);
    R=[r(1), r(2), r(3);
        r(4), r(5), r(6);
        r(7), r(8), r(9)];
    omegahat=omega_to_omegahat(omega);
    dotR=R*omegahat;
    dr=[dotR(1,1); dotR(1,2); dotR(1,3); dotR(2,1); dotR(2,2); dotR(2,3); dotR(3,1); dotR(3,2); dotR(3,3)];
    domega = u;
    dx = [dr; domega];
end