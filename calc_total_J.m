function total_J = calc_total_J(N, R_his, om_his, u_his)
    global Qr Qo R Terminalcost
    total_J=zeros(1,N+1);
    for k=1:N
        R_k     = R_his(3*(k+1)-2:3*(k+1), 1:3);
        omega_k = om_his(1:3,k+1);
        u_km    = u_his(1:3, k);
        if k==N
            Qr=Terminalcost*Qr;
        end
        a=SO3_to_so3(R_k);
        r_k=[a(3,2); a(1,3); a(2,1)];
        Jk=0.5*( r_k'*Qr*r_k + omega_k'*Qo*omega_k + u_km'*R*u_km );
        total_J(k+1)=total_J(k)+Jk;
    end
end