clear all; close all; addpath('./func','./test_mfiles','./plot');

%% simulation settings
global hold_order
hold_order=0; % only 0
state_update_model=1; % 0:same as prediction model(outdated), 1:use ode45(recommended)
specify_grad=0; % only 0

%% output settings
movie_output = 0; % 0:skip, 1:output
movie_save   = 0; % 0:ignore, 1:save
graph_prefix = 'ContSystem_first';
graph_save   = 0;
mkdir('./plot/', graph_prefix)

%% parameters
Duration = 5; % simulation time
qr=10; % weight w.r.t. orientation
qo=1;  % weight w.r.t. ang velocity
r=0.1; % weight w.r.t. inputs

%% set global variable
global Ts Qr Qo R p Terminalcost
Ts = 0.1;             % sample time step
p=10;                 % predict step number
Qr = diag([qr qr qr]); % Weighting Matrix of orientation (which represented by so3)
Qo = diag([qo qo qo]);   % Weighting Matrix of body ang velocity (omega^b)
R = diag([r r r]);    % Weighting Matrix of inputs u (omega_k+1 = omega_k + u*Ts)
Terminalcost=1;

%% set initial state
R0 = rot_y(-5*pi/6)*rot_z(3*pi/4); % initial orientation (in SO3) : random
q0 = SO3_to_q(R0);                 % initial orientation (in quaterinon), same as R0
om0= [0; 0; 0];                    % initial body ang velocity (in R^3)
u0 = [0; 0; 0];
bar_u=repmat(u0, p, 1);

%% prepare history
n = Duration/Ts+1;
R_his    = repmat(R0, n, 1);
q_his    = repmat(q0, 1, n);
om_his   = repmat(om0, 1, n);
u_his    = repmat(bar_u(1:3), 1, n);
t_History = [];
fval_History = [];

%% simulate
if specify_grad == 1
    options = optimoptions('fminunc','Display','none','SpecifyObjectiveGradient',true); %koubai sitei
elseif specify_grad == 0
    options = optimoptions('fminunc','Display','none','SpecifyObjectiveGradient',false);
end
fprintf('Simulation started.  It might take a while...\n')
for ct = 1:(Duration/Ts)
    R_k=R_his(3*ct-2:3*ct, 1:3);
    omega_k=om_his(1:3, ct);
    tic;
    % Nonlinear MPC computation with full state feedback (no state estimator)
    COSTFUN = @(bar_u)calc_J(R_k, omega_k, bar_u);
    %wopt = fmincon(COSTFUN,wopt,[],[],[],[],LB,UB,CONSFUN,options);
    [bar_u, fval] = fminunc(COSTFUN, bar_u, options);

    t_History = [t_History toc];
    
    if state_update_model == 0
       [R_kp, omega_kp] = calc_state_one_step(R_k, omega_k, bar_u(1:3)); 
    elseif state_update_model == 1
       [R_kp, omega_kp] = ContSystem_one_step(R_k, omega_k, bar_u(1:3));
    end
    
    % Save plant states for display.
    fval_History = [fval_History fval];
    q_his(:, ct+1)    = SO3_to_q(R_kp);
    om_his(:, ct+1)   = omega_kp;
    u_his(:, ct+1)    = bar_u(1:3);
    R_his(3*ct+1:3*(ct+1), 1:3) = R_kp;
    fprintf('%d/%d\nfval = %d\n', ct, Duration/Ts, fval);
end
fprintf('Simulation finished!\n')
total_J=calc_total_J(n-1, R_his, om_his, u_his);

%% Output Movie
if movie_output==1
    figure('Position',[100 100 800 600]);
    f1=figure(1);
    clf;
    view(3);
    axis equal;
    axis([-2 2 -2 2 -2 2]);
    axis vis3d;
    grid on;
    yticks([-3:1:3])
    zticks([-3:1:3])
    ax = gca;
    n = Duration/Ts+1;
    for i=1:n
        hcubei{i} = plotcube([1 1.5 0.2], [0 0 0], 0.2);
        ti{i} = hgtransform('Parent',gca);
        cellfun(@(h)set(h,'Parent',ti{i}),hcubei{i});
    end
    hcube = plotcube([1 1.5 0.2], [0 0 0], 0.8);
    ccube = plotcube([0.05 0.05 0.05], [-0.025 -0.025 -0.025], 1.0,[0,0,1]);
    t = hgtransform('Parent',ax);
    cellfun(@(h)set(h,'Parent',t),hcube);
    xlabel('x')
    ylabel('y')
    zlabel('z')
    filename = 'movie_main';
    mov = VideoWriter([strcat('./plot/',graph_prefix,'/',filename) '.avi'], 'Motion JPEG AVI');
    mov.Quality = 100;
    mov.FrameRate = 1/Ts;
    if movie_save==1; open(mov); end
    warning('off','MATLAB:hg:DiceyTransformMatrix')
    text(3.5, 0.5, 1.6, strcat('Sample time=',num2str(Ts)),'FontSize',15);
    text(3.5, 0.5, 1.3, strcat('Pred. horizon=',num2str(p)), 'FontSize',15);
    text(4, 0, 0.1, strcat('R=',num2str(r),'*I'),'FontSize',15);
    text(4, 0, 0.4, strcat('Qo=',num2str(qo),'*I'),'FontSize',15);
    text(4, 0, 0.7, strcat('Q4=',num2str(qr),'*I'),'FontSize',15);
    text(3.5, 0.5, 1.0, strcat('Terminal cost=',num2str(Terminalcost)),'FontSize',15);
    for k=1:n
        t.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];
        %ti{k}.Matrix=[R_his(3*k-2:3*k, 1:3),[0;0;0]; [0,0,0], 1];
        timetext=strcat('t=', num2str(Ts*(k-1), '%10.1f'));
        tx=text(-3.1,3.1,0.5,timetext,'FontSize',22);
        
        drawnow;
        

        if movie_save==1
        frame=getframe(f1);
        writeVideo(mov,frame);
        end
        %pause
        tx.Visible='off';
    end
    if movie_save==1; close(mov); end
    %close all;
end


%% plot result
tspan=0:Ts:Duration;

% calc time plot
f_tictoc=figure('Position', [1260 80 450 400]);
timeavg = sum(t_History)/(Duration/Ts)
timemax = max(t_History)
timemin = min(t_History)
timemed = median(t_History)
boxplot(t_History,'Whisker',10)
ylimmax=(fix(timemax*52)+1)/50;
ylim([0 ylimmax]); grid on;
tx=text(1.1,timemed,strcat('$$t_{med}=$$',num2str(timemed)),'FontSize',13,'Interpreter','latex');
ylabel('Calculation time [s]', 'FontSize',14, 'Interpreter', 'latex')
title(strcat('Boxplot of calculation time'),...
    'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/boxplot'),'epsc')
end

% orientation plot
f_q=figure('Position', [50 80 600 400]);
plot(tspan, q_his, 'LineWidth',2);
grid on;
legend({'$$q_z$$', '$$q_{v1}$$', '$$q_{v2}$$','$$q_{v3}$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Orientation $$q$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_q'),'epsc')
end

% ang velocity plot
f_omega=figure('Position', [650 80 600 400]);
plot(tspan, om_his, 'LineWidth', 2)
grid on;
legend({'$$\omega^b_1$$', '$$\omega^b_2$$', '$$\omega^b_3$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Body Abgular Velocity $$\omega^b$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_omega'),'epsc')
end

% input plot
f_u=figure('Position', [50 600 600 400]);
stairs(tspan, u_his(1,:), 'LineWidth',1.7); hold on;
stairs(tspan, u_his(2,:), 'LineWidth',1.7);
stairs(tspan, u_his(3,:), 'LineWidth',1.7);
grid on;
legend({'$$u_1$$', '$$u_2$$', '$$u_3$$'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Input Acceleration $$u$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_u'),'epsc')
end

% fval plot
f_fval=figure('Position', [650 600 600 400]);
semilogy(tspan(2:end), fval_History, 'LineWidth',2)
grid on;
legend({'fval'}, 'FontSize',13, 'Interpreter', 'latex')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Value of fval', 'FontSize',14, 'Interpreter', 'latex')
ylim([0.8*10^ (-10) 10^2])
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_fval'),'epsc')
end

% total_J plot
f_J=figure('Position', [1260 600 600 400]);
Jm=total_J(end)
plot(tspan, total_J, 'LineWidth',2)
hold on;
grid on;
plot(tspan, repmat(Jm,1,ct+1),'-')
text(0.1,Jm+2,num2str(Jm))
legend({'$$J$$',strcat('$$J_{Max}=$$',num2str(Jm))}, 'FontSize',13, 'Interpreter', 'latex','Location','southeast')
xlabel('Time [s]', 'FontSize',14, 'Interpreter', 'latex')
ylabel('Sum of $$J$$', 'FontSize',14, 'Interpreter', 'latex')
if graph_save==1
saveas(gcf,strcat('./plot/',graph_prefix,'/plot_total_J'),'epsc')
end

%close all;