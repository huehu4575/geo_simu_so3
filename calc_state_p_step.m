% Calc. bar_q, bar_omega, from q_{k+0}, omega_{k+0}, u_{k+0}, ..., u_{k+p-1}
function [bar_R, bar_omega] = calc_state_p_step(R_k, omega_k, bar_u, hold_order)
    global Ts p
    % LET: bar_*=zeros(size,1)
    bar_R=zeros(3*p,3);  % q_k+1, ..., q_k+p
    bar_omega=zeros(3*p,1); % omega_k+1, ..., omega_k+p

    % Calculate *_{k+1} from *_k
    bar_omega(1:3) = omega_k+bar_u(1:3)*Ts;        % omega_{k+1} = omega_k + u_k*Ts
    bar_R(1:3, 1:3) = Rkp_eq_zeroth(R_k,omega_k,bar_omega(1:3));

    % Calculate *_{k+i} from *_{k+i-1}
    for i=2:p
        j=i-1;
        bar_omega(3*i-2:3*i,1) = bar_omega(3*j-2:3*j)+bar_u(3*j-2:3*j)*Ts;
        bar_R(3*i-2:3*i,1:3)  = Rkp_eq_zeroth(bar_R(3*j-2:3*j, 1:3), bar_omega(3*j-2:3*j), bar_omega(3*i-2:3*i,1));
    end
end