Omega_np=[0; rand(3,1)]
Omega_n=[0; rand(3,1)]
O3=[0; rand(3,1)]
T=0.1;

O1=(Omega_np + Omega_n)/2 % Omega_bar
O2=(Omega_np - Omega_n)/T % dot_{Omega}

bunpai = q_product(O1, (O2+O3)) - ( q_product(O1, O2) +q_product(O1, O3) )
q_product(q_product(O1, O2), O1)-q_product(q_product(O1, O1), O2)


O=O1-0.5*T*O2;
res2=q_product(O,O) - (q_product(O1, O1)-T/2*(q_product(O1, O2)+q_product(O2, O1))+(T^2)/4*q_product(O2,O2))
res3=q_product(O, q_product(O,O)) - ( q_product(O1, q_product(O1, O1))-T*q_product(q_product(O1, O1), O2)-T/2*q_product(q_product(O1, O2), O1)...
    + (T^2)/2*(q_product(q_product(O1, O2), O2)) + (T^2)/4*q_product(q_product(O2, O1), O2)-(T^3)/8*q_product(q_product(O2, O2), O2)  )
alt3=q_product(O, q_product(O,O)) - ( q_product(O1, q_product(O1, O1))-T*1.5*q_product(q_product(O1, O1), O2)...
    + (T^2)*(3/4)*(q_product(q_product(O1, O2), O2)) +(T^3)/8*q_product(q_product(O2, O2), O2)  )