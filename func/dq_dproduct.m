function out = dq_dproduct(dq1, dq2)
    out=q_cproduct(dq1.p, dq2.p)+q_cproduct(dq1.d, dq2.d);
end