%  zero-th hold equation
function R_kp = Rkp_eq_zeroth(R_k,omega_k,~)
    global Ts
    % q_{k+1} = q_k (otimes) q_omega
    if isrow(omega_k)
        omega_k=omega_k.';
    end
    a=omega_to_omegahat(omega_k);
    R_o=expm(a*Ts);
    R_kp=R_k*R_o;
end