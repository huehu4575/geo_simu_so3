function out = q_dproduct(q1, q2)
    q1.z=q1(1);
    q1.v=q1(2:4);
    q2.z=q2(1);
    q2.v=q2(2:4);
    out=q1.z*q2.z+q1.v'*q2.v;
end