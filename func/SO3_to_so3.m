function a = SO3_to_so3(R)
    theta=acos((trace(R)-1)/2);
    xi=1/(2*sin(theta))*[R(3,2)-R(2,3);
                         R(1,3)-R(3,1);
                         R(2,1)-R(1,2)];
    xi_hat=omega_to_omegahat(xi);
    a = theta*xi_hat;    
end