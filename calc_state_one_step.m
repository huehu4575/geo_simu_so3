%No more used
function [R_kp, omega_kp] = calc_state_one_step(R_k, omega_k, u_k, hold_order)
    global Ts
    % prepare *_kp
    R_kp=eye(3);  % R_k+1
    omega_kp=zeros(3,1); % omega_k+1

    % Calculate *_{k+1} from *_k
    omega_kp = omega_k+u_k(1:3)*Ts;         % omega_{k+1} = omega_k + u_k*Ts
    R_kp = Rkp_eq_zeroth(R_k,omega_k,omega_kp);  % R_{k+1}
end