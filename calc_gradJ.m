function gradJ = calc_gradJ(breve_q, bar_q, bar_omega, bar_u, barQ4, barQo, barR)
    global Ts p
    dodu = zeros(3*p);
    dqdo = zeros(4*p, 3*p);
%   dqdq = zeros(4*p); % =Identity Matrix

    %% calculate dodu
    for i=1:p
       for j=1:i
              dodu(3*i-2:3*i, 3*j-2:3*j)=Ts*eye(3);
       end
    end
    
    %%
    A=zeros(4, 4*p);
    A(1:4, 1:4)=eye(4);
    A(1:4, 4*p-3:4*p)=eye(4);
    B=zeros(4, 3*(p-1));
    for i = 2:(p-1)
        omega_k = bar_omega(3*i-2:3*i);
        A(1:4, 4*i-3:4*i) = calc_Ak(omega_k);
    end
    for i = 1:(p-1)
        omega_k = bar_omega(3*i-2:3*i);
        q_k  = bar_q(4*i-3:4*i);
        B(1:4, 3*i-2:3*i) = calc_Bk(q_k, omega_k);
    end
    
    %% calculate dqdo
    for j=1:(p-1)
        i=j+1;
        dqdo(4*i-3:4*i, 3*j-2:3*j)=B(1:4, 3*j-2:3*j);
        for i=(j+2):p
            dqdo(4*i-3:4*i, 3*j-2:3*j) = A(1:4, 4*(i-1)-3:4*(i-1))*dqdo(4*(i-1)-3:4*(i-1), 3*j-2:3*j);
        end
    end
 
    gradJ = breve_q'*barQ4*dqdo*dodu+bar_omega'*barQo*dodu+(bar_u).'*barR;
end